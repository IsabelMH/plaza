$(function () {
  	$('[data-toggle="tooltip"]').tooltip();
  	$('[data-toggle="popover"]').popover();
  	$('.carousel').carousel({
  		interval: 4500
 	});
 	$('#proovedor').on('show.bs.modal', function (e){
		console.log('El modal contacto proovedor está activo');
		$('#ContactoBton').removeClass('btn-color');
		$('#ContactoBton').addClass('btn-default');
		$('#ContactoBton').prop('disabled', true);
	});
	$('#proovedor').on('shown.bs.modal', function (e){
		console.log('El modal contacto proovedor se mostró');
	});
	$('#proovedor').on('hide.bs.modal', function (e){
		console.log('El modal contacto proovedor se oculta');
	});
	$('#proovedor').on('hidden.bs.modal', function (e){
		console.log('El modal contacto proovedor se ocultó');
		$('#ContactoBton').addClass('btn-color');
		$('#ContactoBton').prop('disabled', false);
	});

});

